# Exportar calificaciones de Moodle-UHU a Actas Vía WEB

## Objeto

Plugin de exportación de las calificaciones de una asignatura de la
plataforma de enseñanza virtual Moodle-UHU a una hoja de cálculo Excel.

La hoja de cálculo resultante tiene el formato requerido para que
pueda ser importada directamente desde la aplicación Actas Vía Web.

## Descripción y uso

Entre las opciones de exportación de las calificaciones de una
asignatura que ofrece Moode-UHU se encuentra la opción "Actas",
correspondiente a este plugin.

Al elegir dicha opción, aparece una pantalla con un texto explicativo
debajo del cual hay un botón que permite descargar una hoja de cálculo
con las calificaciones de los alumnos matriculados en la referida
asignatura.

Las calificaciones corresponden al item "Total del curso" y se
incorporan en la hoja de cálculo con 2 decimales de precisión, que es
la que utiliza la aplicación de actas en su informes.
