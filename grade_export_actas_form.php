<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    gradeexport_actas
 * @author     Jesús Lago Macía - Universidad de Huelva
 * @copyright  2020 Jesús Lago Macía <lago@uhu.es>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once $CFG->libdir.'/formslib.php';

class grade_export_actas_form extends moodleform {
    function definition() {
        global $CFG, $COURSE;

        $isdeprecatedui = false;

        $mform =& $this->_form;

        $mform->addElement('html', get_string('actas:textoex', 'gradeexport_actas'));

        $mform->addElement('hidden', 'id', $COURSE->id);
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons(false, get_string('actas:export', 'gradeexport_actas'));
    }
}

