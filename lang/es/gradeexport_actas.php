<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public 
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * Strings for component 'gradeexport_actas', language 'es'
 *
 * @package    gradeexport_actas
 * @author     Jesús Lago Macía - Universidad de Huelva
 * @copyright  2020 Jesús Lago Macía <lago@uhu.es>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['eventgradeexported'] = 'Calificaciones exportadas por Actas';
$string['pluginname'] = 'Actas';
$string['privacy:metadata'] = 'El plugin de exportación de calificaciones Actas no almacena ningún dato personal.';
$string['actas:publish'] = 'Publicar las calificiones exportadas por Actas';
$string['actas:view'] = 'Utilizar Actas para exportar calificaciones';
$string['actas:export'] = 'Exportar';
$string['actas:textoex'] = '<div><h3>Se exporta el libro de calificaciones del presente curso al
                           formato de hoja de cálculo que puede importarse
                           desde la aplicación de actas de la UHU.</h3>
                           </br>
                           <h3>La calificación se toma del item "Total del curso", esta debe ser numérica y estar
                           escalada entre 0 y 10. El valor de la calificación se expresa con 2 decimales, la
                           precisión utilizada en los informes emitidos por la aplicación de actas.</h3>
                           </br>
                           &copy; Jesús Lago Macía.
                           <hr>
                           </div>';
