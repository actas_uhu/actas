<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'gradeexport_actas', language 'en'
 *
 * @package    gradeexport_actas
 * @author     Jesús Lago Macía - Universidad de Huelva
 * @copyright  2020 Jesús Lago Macía <lago@uhu.es>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['eventgradeexported'] = 'Actas grade exported';
$string['pluginname'] = 'Actas';
$string['privacy:metadata'] = 'The Actas grade export plugin does not store any personal data.';
$string['actas:publish'] = 'Publish Actas grade export';
$string['actas:view'] = 'Use Actas grade export';
$string['actas:export'] = 'Export';
$string['actas:textoex'] = '<div><h3>The grade book for this course is exported to the spreadsheet
                            format that can be imported from the UHU application "Actas Vía Web".</h3>
                           </br>
                           <h3>The grade is taken from the item "Course total", it must be numerical and
                            must be scaled between 0 and 10. The value of the grade is expressed with 2
                            decimals, the precision used in the reports by the application "Actas Vía Web".</h3>
                           </br>
                           &copy; Jesús Lago Macía.
                           <hr>
                           </div>';
