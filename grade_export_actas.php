<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Function used in export process.
 *
 * @package    gradeexport_actas
 * @author     Jesús Lago Macía - Universidad de Huelva
 * @copyright  2020 Jesús Lago Macía <lago@uhu.es>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/grade/export/lib.php');
require_once($CFG->dirroot.'/grade/querylib.php');

class grade_export_actas extends grade_export {

    public $plugin = 'actas';

    /**
     * Constructor should set up all the private variables ready to be pulled
     * @param object $course
     * @param int $groupid id of selected group, 0 means all
     * @param stdClass $formdata The validated data from the grade export form.
     */
    public function __construct($course, $groupid, $formdata) {
        parent::__construct($course, $groupid, $formdata);

        // Overrides.
        $this->usercustomfields = true;
    }

    /**
     * To be implemented by child classes
     */
    public function print_grades() {
        global $CFG;
        require_once($CFG->dirroot.'/lib/excellib.class.php');

        // Calculate file name
        $shortname = format_string($this->course->shortname, true, array('context' => context_course::instance($this->course->id)));
        $downloadfilename = clean_filename("$shortname\_actas.xls");
        // Creating a workbook
        $workbook = new MoodleExcelWorkbook("-");
        // Sending HTTP headers
        $workbook->send($downloadfilename);
        // Adding the worksheet
        $myxls = $workbook->add_worksheet($strgrades);

        // Escribir cabeceras
        $formatb =& $workbook->add_format();
        $formatb->set_bg_color('cyan');
        $myxls->write_string(0, 0, "Nombre alumno", $formatb);
        $myxls->write_string(0, 1, "Número de identificación", $formatb);
        $myxls->write_string(0, 2, "Nota numérica", $formatb);
        $myxls->write_string(0, 3, "Calificación del alumno", $formatb);
        $myxls->set_column(0, 0, 45);
        $myxls->set_column(1, 1, 20);
        $myxls->set_column(2, 2, 10);
        $myxls->set_column(3, 3, 30);

        // Escribir datos y calificación de usuario
        $i = 0;
        $gui = new graded_users_iterator($this->course, $this->columns, $this->groupid);
        $gui->require_active_enrolment($this->onlyactive);
        $gui->allow_user_custom_fields($this->usercustomfields);
        $gui->init();
        while ($userdata = $gui->next_user()) {
            $i++;
            $user = $userdata->user;
            $nuser = strtoupper($user->lastname) . ", " . strtoupper($user->firstname);
            $myxls->write_string($i, 0, $nuser);
            $myxls->write_string($i, 1, $user->idnumber);
            $result = grade_get_course_grade($user->id, $this->course->id);
            $myxls->write_number($i, 2, round($result->grade,2));
        }

        $gui->close();
    /// Close the workbook
        $workbook->close();

        exit;
    }
}
